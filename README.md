[![License](https://img.shields.io/badge/License-MIT-purple?style=flat-square)](LICENSE)
[![Open Source](https://img.shields.io/badge/Open%20Source-Yes-blue?style=flat-square)](https://opensource.org/)
[![Maintenance](https://img.shields.io/badge/Maintained-No-red.svg?style=flat-square)](https://gitlab.com/ThomasCat/stockanalyze/-/network/main)

# Camcanvas
Camcanvas is a simple project I wrote for my Digital Image Processing class ([18CS651](https://vtu.ac.in/pdf/2014syll/cs.pdf)). It is a Python program that automatically detects contrast and motion to generate lines that are continuous, essentially turning anything into a drawing surface.
<img src='MediaNStuffz/1.png'>

## SETUP
1. First, make sure your machine has the following:
    - An [internet connection](http://fixwifi.it/)
    - [Python 3](https://www.python.org/downloads/)
    - The [Python package manager](https://pip.pypa.io/en/stable/installing/)
    - [OpenCV](https://opencv.org)

2. Download this project archive and extract it _or_ clone this project using `git clone https://gitlab.com/ThomasCat/stockanalyze.git`

## USAGE
Run Camcanvas by typing 
```
python camcanvas.py
```
in terminal. 

The output should look similar to this...

<img src='MediaNStuffz/2.png'>

## LIBRARIES USED
- Python – Python is a general purpose language used for digital image processing, which is easy to
use due to its simple syntax, academic research, large AI library support and statistical models.
- OpenCV – OpenCV is a library used for digital image processing and computer vision and it plays a
major role in real-time operations to identify objects, faces, or even handwriting of a human.
- NumPy – NumPy is a Python library used for working with arrays. It also has functions for working
in domain of linear algebra, fourier transform, and matrices.

## LICENSE
Multimedia licensed under [![License: CC BY-NC-SA 4.0](https://licensebuttons.net/l/by-nc-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/) 

[Copyright © 2021 Owais Shaikh](LICENSE)
